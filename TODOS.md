The goal is to implement a small react app to display a list (or table) of news with a few properties like title (= link with url), author, and number of comments.

The basic app should
- List hackernews articles (use the most important properties like author, title, num_comments, etc).
- Has a search input field to query articles on the server using the "query" parameter
- Has some basic styling in place.

Additional features
- Master-Detail view: When you click on an entry – open a detail view of that article
- Has a button to load more articles and append these at the bottom of the table.
- A “dismiss” button to remove entries from the list?
- Add some tests?
 

Search/ List News:
https://hn.algolia.com/api/v1/search

Parameters:
- query= … search term
- page=… page offset, default 0
- hitsPerPage=… #entries to fetch per page.


Single Story (for Detail Drilldown):
https://hn.algolia.com/api/v1/items/:id